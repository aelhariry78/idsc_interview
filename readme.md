#### kafka 
  - getting no fault tolerance and helps process the data without worrying about the stream getting disconnected.

#### notes 
  - **Sentiment Analysis** also known as opinions mining
  - **Sentiment Analysis Approches** 
    - Rule/lexicon-based approach
    - Automatic


### references 
  - [kafka with twitter](https://lorenagongang.com/getting-started-with-kafka-twitter-streaming-with-apache-kafka)
  - [intro part1](https://lorenagongang.com/sentiment-analysis-on-streaming-twitter-data-using-kafka-spark-structured-streaming-and-python-part-1)
  - [intro part2](https://lorenagongang.com/sentiment-analysis-on-st-reaming-twitter-data-using-kafka-spark-structured-streaming-and-python-part-2)
  - [intro part3](https://lorenagongang.com/sentiment-analysis-on--streaming-twitter-data-using-kafka-spark-structured-streaming-and-python-part-3)
  - [medium](https://medium.com/@lorenagongang/sentiment-analysi-s-on-streaming-twitter-data-using-kafka-spark-structured-streaming-python-part-b27aecca697a)
  - [sentiment analysis](https://towardsdatascience.com/sentim-ent-analysis-on-streaming-twitter-data-using-spark-structured-streaming-python-fc873684bfe3)
  - [github code](https://github.com/g-lorena/twittersentimentanalysis)


  
#### Twitter credentials
  - **API Key** : `P82c9M01aksapDxlA71SMexDh`  
  - **API Key Secret** : `n0D3pIBlllGtLR4dT1w0FM8jSbacF7WOo1o2R086X0YRUHUSaJ`  
  - **Bearer Token**: `AAAAAAAAAAAAAAAAAAAAAEjJlAEAAAAAh9K5GaGENFdSrrIjSXMflQeoWBQ%3DZcvnVn3vTv1Lro99hrJ5Qlmrr9lpbo6bxpDO4LNRQlWE0Ml7r0`  
  - **Access Token**: `1344973074-ozjuXNgsDh90Viv27Md7qcdf6WACukcDT4LTqHC`
  - **Access Token Secret**: `MKIsY1Au2lygaEludB0o5DNE0WcgjZGPOEtA9EXp6tHWH`
  
  P82c9M01aksapDxlA71SMexDh  
  n0D3pIBlllGtLR4dT1w0FM8jSbacF7WOo1o2R086X0YRUHUSaJ  
  AAAAAAAAAAAAAAAAAAAAAEjJlAEAAAAAh9K5GaGENFdSrrIjSXMflQeoWBQ%3DZcvnVn3vTv1Lro99hrJ5Qlmrr9lpbo6bxpDO4LNRQlWE0Ml7r0  